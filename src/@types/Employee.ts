export type Employeekeys =
  | "id"
  | "first_name"
  | "last_name"
  | "email"
  | "gender"
  | "ip_address";

interface EmployeeObject {
  isChecked: boolean;
  name: string;
}

export type PropertiesKeys = Record<Employeekeys, EmployeeObject>;

export interface PropertyKeys {
  id: EmployeeObject;
  first_name: EmployeeObject;
  last_name: EmployeeObject;
  email: EmployeeObject;
  gender: EmployeeObject;
  ip_address: EmployeeObject;
}
