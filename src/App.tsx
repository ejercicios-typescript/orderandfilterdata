import { useState } from "react";
import "./App.css";
import SelectData from "./components/SelectData";
import { Employee, employees } from "./data/employees";
import { Car, cars } from "./data/cars";
import { search } from "./utils/search";
import InputSearch from "./components/InputSearch";
import ItemsFilter from "./components/ItemsFilter";
import {
  CreateObjectForCheckboxFilter,
  PropertiesKeys,
} from "./utils/createObjectForCheckboxFilter";
import { addressCollection, AddressInformation } from "./data/addressData";

export interface CollectionData {
  employees: Array<Employee>;
  cars: Array<Car>;
  addressData: Array<AddressInformation>;
}

export type KeyOfCollectionData = keyof CollectionData;

type OrTypeCollection = PropertiesKeys<Employee | Car | AddressInformation>;

function App() {
  const provider: CollectionData = {
    employees,
    cars,
    addressData: addressCollection,
  };

  const [query, setQuery] = useState<string>("");
  const [propertiesArray, setpropertiesArray] = useState<OrTypeCollection>(
    CreateObjectForCheckboxFilter({ objectModel: cars[0] })
  );
  const [searchBy, setSearchBy] = useState<Array<keyof OrTypeCollection>>([]);
  const [dataView, setDataView] = useState<Array<OrTypeCollection>>(cars);

  const handleChangeData = (dataSelected: KeyOfCollectionData) => {
    const dataSelectedArray = provider[dataSelected];
    setpropertiesArray(
      CreateObjectForCheckboxFilter({ objectModel: dataSelectedArray[0] })
    );
    setDataView(dataSelectedArray);
  };

  return (
    <div className="App">
      <div className="card">
        <SelectData allData={provider} selectData={handleChangeData} />
        <InputSearch query={query} setQuery={setQuery} />
        <ItemsFilter
          propertiesArray={propertiesArray}
          setpropertiesArray={setpropertiesArray}
          setSearchBy={setSearchBy}
        />
        {dataView
          .filter((item) => search(item, searchBy, query))
          .map((item, index) => {
            return (
              // <p>{item.first_name}</p>;
              <pre key={index}>
                <code>{JSON.stringify(item, null, 2)}</code>
              </pre>
            );
          })}
      </div>
    </div>
  );
}

export default App;
