// import { PropertiesKeys } from "../@types/Employee";

interface ObjectForCheckbox<U> {
  objectModel: U;
}

interface StructureCheckbox {
  isChecked: boolean;
  name: string;
}

export type PropertiesKeys<V> = Record<keyof V, StructureCheckbox>;

export const CreateObjectForCheckboxFilter = <T extends {}>({
  objectModel,
}: ObjectForCheckbox<T>): PropertiesKeys<T> => {
  const arrayKeys = Object.keys(objectModel) as Array<keyof T>;
  const createObjectState: any = {};
  arrayKeys.forEach((key) => {
    Object.assign(createObjectState, {
      [key]: { isChecked: false, name: key },
    });
  });
  return createObjectState;
};
