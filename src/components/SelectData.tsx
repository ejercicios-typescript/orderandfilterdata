import React from "react";
import { CollectionData, KeyOfCollectionData } from "../App";
import { Car } from "../data/cars";
import { Employee } from "../data/employees";

interface Props {
  allData: CollectionData;
  selectData: (selectedData: KeyOfCollectionData) => void;
}

const SelectData = ({ allData, selectData }: Props) => {
  return (
    <select
      name=""
      id=""
      onChange={(e) => selectData(e.target.value as KeyOfCollectionData)}
    >
      <option value="">Elige una opción</option>
      {Object.keys(allData).map((data) => (
        <option key={data} value={data}>
          {data}
        </option>
      ))}
    </select>
  );
};

export default SelectData;
