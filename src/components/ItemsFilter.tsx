import React, { FC } from "react";
import { PropertiesKeys } from "../utils/createObjectForCheckboxFilter";

interface Props<T> {
  propertiesArray: PropertiesKeys<T>;
  setpropertiesArray: React.Dispatch<React.SetStateAction<PropertiesKeys<T>>>;
  setSearchBy: React.Dispatch<React.SetStateAction<Array<keyof T>>>;
}

const ItemsFilter = <T extends unknown>({
  propertiesArray,
  setpropertiesArray,
  setSearchBy,
}: Props<T>) => {
  const objectKeys = Object.keys(propertiesArray) as Array<keyof T>;
  return objectKeys.length <= 0 ? (
    <h1>No hay información</h1>
  ) : (
    <div className="group_filter">
      {objectKeys.map((property) => {
        const objectIterable = propertiesArray[property as keyof T];
        return (
          <div className="item_group_filter" key={`${property.toString()}`}>
            <input
              type="checkbox"
              name={`${objectIterable.name}`}
              id={`${objectIterable.name}`}
              value="property"
              onChange={({ target }) => {
                target.checked
                  ? setSearchBy((prevSearch) => [
                      ...prevSearch,
                      target.name as keyof T,
                    ])
                  : setSearchBy((prevSearch) =>
                      prevSearch.filter((p) => p !== target.name)
                    );
                setpropertiesArray((prevState) => {
                  return {
                    ...prevState,
                    [target.name]: {
                      ...prevState[target.name as keyof T],
                      isChecked: target.checked,
                    },
                  };
                });
              }}
            />
            <label htmlFor={`${objectIterable.name}`}>
              {objectIterable.name.replaceAll("-", " ")}
            </label>
          </div>
        );
      })}
    </div>
  );
};

export default ItemsFilter;
